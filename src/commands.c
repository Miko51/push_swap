/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   commands.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/03 13:42:25 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/04/09 12:37:12 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/stack.h"

void	multi_command(t_node *a, t_node *b, char *com)
{
	if (ft_strcmp(com, "rr"))
	{
		rotate(a, 'a', 0);
		rotate(b, 'b', 0);
		write(1, "rr\n", 3);
	}
	else if (ft_strcmp(com, "rrr"))
	{
		rrotate(a, 'a', 0);
		rrotate(b, 'b', 0);
		write(1, "rrr\n", 4);
	}
	else if (ft_strcmp(com, "ss"))
	{
		swap(a, 'a');
		swap(b, 'b');
	}
}

void	swap(t_node *s, char c)
{
	int	temp;

	print_command("s", c);
	temp = s->data;
	s->data = s->next->data;
	s->next->data = temp;
}

t_node	*push(int data, t_node *b, char c)
{
	t_node	*temp;

	temp = (t_node *) malloc(sizeof(t_node));
	print_command("p", c);
	if (!b)
	{
		temp->data = data;
		temp->prev = NULL;
		temp->next = NULL;
		b = temp;
	}
	else
	{
		temp->data = data;
		temp->prev = NULL;
		temp->next = b;
		b->prev = temp;
		b = temp;
	}
	return (b);
}

void	rrotate(t_node *s, char c, int f)
{
	size_t	size;
	int		temp;
	int		tempindex;

	size = size_stack(s);
	if (f)
		print_command("rr", c);
	if (s != NULL)
	{
		while (s->next)
			s = s->next;
		while (size > 1 && s->prev)
		{
			temp = s->data;
			tempindex = s->index;
			s->data = s->prev->data;
			s->index = s->prev->index;
			s->prev->data = temp;
			s->prev->index = tempindex;
			s = s->prev;
		}
	}
}

void	rotate(t_node *s, char c, int f)
{
	size_t	size;
	int		temp;
	int		tempindex;
	t_node	*iter;

	size = size_stack(s);
	iter = s;
	if (f)
		print_command("r", c);
	while (size > 1 && s->next)
	{
		temp = s->data;
		tempindex = s->index;
		s->data = s->next->data;
		s->next->data = temp;
		s->index = s->next->index;
		s->next->index = tempindex;
		s = s->next;
	}
}

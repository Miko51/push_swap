/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_utils2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/03 13:45:37 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/04/09 12:39:08 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/stack.h"

int	control_element(t_node *a, int *space2, int j)
{
	int	c;
	int	c2;

	c = is_up(a, space2, j);
	c2 = is_down(a, space2, j);
	if (c >= c2)
		return (c);
	return (c2 * -1);
}

void	push2(t_node **a, t_node **b, int *i, int *c)
{
	*b = push((*a)->data, *b, 'b');
	*a = pop(*a);
	(*i)++;
	(*c)--;
}

int	rotate2(t_node *a, t_node *b, int c)
{
	int	j;

	j = index_loc(b, size_stack(b));
	if (c > 0)
	{
		rotate(a, 'a', 1);
		c--;
	}
	else if (c < 0)
	{
		rrotate(a, 'a', 1);
		c++;
	}	
	return (c);
}

void	rotate3(t_node *s, int size)
{	
	int	j;

	j = index_loc(s, size);
	if (j < 0)
	{
		j *= -1;
		while (j != 0)
		{
			rrotate(s, 'b', 1);
			j--;
		}
	}
	else
	{
		while (j != 0)
		{
			rotate(s, 'b', 1);
			j--;
		}
	}
}

void	print_command(char *arr, char node)
{
	if (ft_strcmp(arr, "r") == 0 && node == 'a')
		ft_putstr_fd("ra\n", 1);
	else if (ft_strcmp(arr, "rr") == 0 && node == 'a')
		ft_putstr_fd("rra\n", 1);
	else if (ft_strcmp(arr, "rr") == 0 && node == 'b')
		ft_putstr_fd("rrb\n", 1);
	else if (ft_strcmp(arr, "r") == 0 && node == 'b')
		ft_putstr_fd("rb\n", 1);
	else if (ft_strcmp(arr, "s") == 0 && node == 'a')
		ft_putstr_fd("sa\n", 1);
	else if (ft_strcmp(arr, "s") == 0 && node == 'b')
		ft_putstr_fd("sb\n", 1);
	else if (ft_strcmp(arr, "p") == 0 && node == 'a')
		ft_putstr_fd("pa\n", 1);
	else if (ft_strcmp(arr, "p") == 0 && node == 'b')
		ft_putstr_fd("pb\n", 1);
}

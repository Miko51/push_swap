/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/03 13:44:28 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/04/09 12:40:08 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/stack.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char			*temp;
	unsigned long	i;

	if (!s || start >= ft_strlen((char *)s))
		return (NULL);
	temp = (char *)malloc(sizeof(char) * (len + 1));
	i = 0;
	if (!temp)
		return (NULL);
	s += start;
	while (i < len && *s)
	{
		temp[i] = ((char *)s)[i];
		i++;
	}
	temp[i] = 0;
	return (temp);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_free.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/16 17:41:16 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/04/09 12:40:41 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/stack.h"

void	free_list(t_node *s)
{
	t_node	*temp;

	while (s)
	{
		temp = s;
		s = s->next;
		free(temp);
	}
}

void	free_array(char **s)
{
	int		i;
	char	**s2;

	i = 0;
	s2 = s;
	while (s[i])
		free(s[i++]);
	free(s2);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/02 16:55:43 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/04/09 12:38:16 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/stack.h"

int	islinear(t_node *a)
{
	while (a->next)
	{
		if (a->data > a->next->data)
			return (0);
			a = a->next;
	}
	return (1);
}

static int	error_free(char **args, t_node **a, int f, int err)
{
	if (err)
		write(1, "Error\n", 6);
	if (f)
		free_array(args);
	free_list(*a);
	return (0);
}

static int	control_args_in(char **args, t_node **a, int f)
{
	if (!control_args(args))
	{
		write(1, "Error\n", 6);
		if (f)
			free_array(args);
		return (0);
	}
	*a = compile_list(args);
	if (!(*a))
	{
		write(1, "Error\n", 6);
		if (f)
			free_array(args);
		return (0);
	}
	if (!is_same(*a))
		return (error_free(args, a, f, 1));
	if (size_stack(*a) == 1 || islinear(*a))
		return (error_free(args, a, f, 0));
	return (1);
}

void	two_and_upper(char **argv, int argc)
{
	t_node	*a;
	char	**args;

	if (argc == 2)
	{
		args = ft_split(argv[1], ' ');
		if (control_args_in(args, &a, 1))
		{
			sort_index(a);
			a = insert(a, NULL);
			free_list(a);
			free_array(args);
		}
	}
	else if (argc > 2)
	{
		if (control_args_in(&argv[1], &a, 0))
		{
			sort_index(a);
			a = insert(a, NULL);
			free_list(a);
		}
	}
}

int	main(int argc, char **argv)
{
	if (argc == 1)
		return (0);
	two_and_upper(argv, argc);
	return (1);
}

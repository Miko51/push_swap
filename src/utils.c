/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/02 15:45:23 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/04/09 12:41:52 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/stack.h"

void	create(t_node *s, char *str, t_node *prev, t_node *next)
{
	s->data = ft_atoi(str);
	s->prev = prev;
	s->next = next;
}

int	minmax_control(char *s, t_node *temp, t_node *a)
{
	if (ft_strlen(s) > 2 && (temp->data == 0 || temp->data == -1))
	{
		free(temp);
		if (a)
			free_list(a);
		return (1);
	}
	return (0);
}

t_node	*compile_list(char **s)
{
	t_node	*a2;
	t_node	*temp;
	t_node	*temp2;
	int		i;

	i = 0;
	temp2 = (t_node *) malloc(sizeof(t_node));
	create(temp2, s[i], NULL, NULL);
	if (minmax_control(s[i++], temp2, NULL))
		return (0);
	a2 = temp2;
	while (s[i])
	{
		temp = (t_node *) malloc(sizeof(t_node));
		create(temp, s[i], a2, NULL);
		if (minmax_control(s[i], temp, temp2))
			return (0);
		a2->next = temp;
		a2 = temp;
		i++;
	}
	return (temp2);
}

int	size_stack(t_node *a)
{
	int	i;

	i = 0;
	while (a)
	{
		i++;
		a = a->next;
	}
	return (i);
}

void	sort_index(t_node *s)
{
	t_node	*iter;
	t_node	*root;
	int		i;

	iter = s;
	root = s;
	i = 0;
	while (root)
	{
		root->index = 0;
		root->index2 = i;
		iter = s;
		while (iter)
		{
			if (root->data > iter->data)
				root->index++;
			iter = iter->next;
		}
		root = root->next;
		i++;
	}
}

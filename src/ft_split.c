/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/03 13:44:02 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/04/09 12:39:49 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/stack.h"

static size_t	len_c(char const *s, char c)
{
	size_t	i;
	size_t	count;

	count = 0;
	i = 0;
	while (s[i])
	{
		if (s[i++] != c)
		{
			count++;
			while (s[i] && s[i] != c)
				i++;
		}
	}
	return (count);
}

char	**ft_split(char const *s, char c)
{
	size_t		i;
	size_t		j;
	size_t		k;
	char		**temp;

	i = 0;
	j = 0;
	k = 0;
	temp = (char **)malloc(sizeof(char *) * (len_c(s, c) + 1));
	if (!temp)
		return (NULL);
	while (s[i] && k < len_c(s, c))
	{
		if (s[i] != c)
		{
			j = i++;
			while (s[i] && s[i] != c)
				i++;
			temp[k] = ft_substr(s, j, i - j);
			k++;
		}
			i++;
	}
	temp[k] = NULL;
	return (temp);
}

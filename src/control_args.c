/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   control_args.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/03 16:36:58 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/04/09 12:39:32 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/stack.h"

int	ft_isdigit(int c)
{
	if (c >= '0' && c <= '9')
	{
		return (1);
	}
	return (0);
}

int	arr_isdigit(char *s)
{
	int	i;

	i = 0;
	while (s[i])
	{
		if (!ft_isdigit(s[i]))
		{
			if (s[i] != '-' && s[i] != '+')
				return (0);
		}
		i++;
	}
	return (1);
}

int	is_same(t_node *s)
{
	t_node	*iter;
	t_node	*iter2;

	sort_index(s);
	iter = s;
	while (iter)
	{
		iter2 = s;
		while (iter2)
		{
			if (iter->data == iter2->data && iter->index2 != iter2->index2)
				return (0);
			iter2 = iter2->next;
		}
		iter = iter->next;
	}
	return (1);
}

int	control_args(char **args)
{
	int	i;

	i = 0;
	while (args[i])
	{
		if (!arr_isdigit(args[i]))
		{
			return (0);
		}
		i++;
	}
	return (1);
}

t_node	*lst_last(t_node *s)
{
	if (s)
	{
		while (s->next)
			s = s->next;
	}
	return (s);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   insert.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/02 16:15:48 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/04/09 12:41:35 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/stack.h"

int	again_count(t_node *a, int space, int *space2, int j)
{
	int	c;

	c = control_element(a, space2, j);
	if (!c)
	{
		space2[0] += space;
		space2[1] += space;
		c = control_element(a, space2, j);
	}
	return (c);
}

void	res(int *space2, int space)
{
	space2[0] = space;
	space2[1] = 0;
}

t_node	*insert2(t_node *a, t_node *b, int space, int size)
{
	int	i;
	int	j;
	int	c;
	int	space2[2];

	i = 0;
	j = space;
	res(space2, space);
	c = 0;
	while (a && i < size)
	{
		if (a->index >= j - space && a->index < j)
			push2(&a, &b, &i, &c);
		else if (c)
			c = rotate2(a, b, c);
		else if (i == j)
			j += space;
		else if (space2[0] > size_stack(a))
			res(space2, space);
		if (!c && a)
			c = again_count(a, space, space2, j);
	}
	return (b);
}

t_node	*insert3(t_node *a, t_node *b)
{
	int	size;

	size = size_stack(b) - 1;
	while (b && size != -1)
	{
		if (b->index == size)
		{
			a = push(b->data, a, 'a');
			b = pop(b);
			size--;
		}
		else
		{
			rotate3(b, size);
		}
	}
	return (a);
}

t_node	*insert(t_node *a, t_node *b)
{
	int	size;

	size = size_stack(a);
	sort_index(a);
	if (size == 100 || size == 500)
	{
		if (size == 100)
			b = insert2(a, b, 15, size);
		else if (size == 500)
			b = insert2(a, b, 40, size);
		a = NULL;
		sort_index(b);
		a = insert3(a, b);
		size = size_stack(b);
	}
	else
		a = other_variation(a, b);
	sort_index(a);
	return (a);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/02 14:57:44 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/04/09 12:38:42 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/stack.h"

t_node	*pop(t_node *s)
{
	t_node	*temp;

	if (s != NULL)
	{
		temp = s;
		s = s->next;
		if (s)
			s->prev = NULL;
		free(temp);
		return (s);
	}
	return (NULL);
}

int	index_loc_down(t_node *s, int index)
{
	int	i;
	int	j2;
	int	size;

	i = -1;
	j2 = 1;
	size = size_stack(s);
	s = lst_last(s);
	i = -1;
	while (s && ++i < size / 2 && index != s->index)
	{
		j2++;
		s = s->prev;
	}
	return (j2);
}

int	index_loc(t_node *s, int index)
{
	int	i;
	int	j;
	int	j2;
	int	size;

	i = -1;
	j = 0;
	j2 = index_loc_down(s, index);
	size = size_stack(s);
	while (s && index != s->index && ++i < size / 2)
	{
		j++;
		s = s->next;
	}
	if ((j < j2 || j == j2) && (j != 0 || j2 != 0))
		return (j);
	return (j2 * -1);
}

int	is_up(t_node *s, int *space2, int j)
{
	int	i;
	int	j2;

	i = 0;
	j2 = 0;
	while (s && j2 < space2[0])
	{
		if (s->index < j && s->index >= j - (space2[0] - space2[1]))
			i++;
		s = s->next;
		j2++;
	}
	return (i);
}

int	is_down(t_node *s, int *space2, int j)
{
	int	i;
	int	i2;

	i = 0;
	i2 = 0;
	while (s->next)
		s = s->next;
	while (s && i2 < space2[0])
	{
		if (s->index < j && s->index >= j - (space2[0] - space2[1]))
			i++;
		s = s->prev;
		i2++;
	}
	return (i);
}

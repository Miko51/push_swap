/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   other.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/09 18:16:14 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/04/09 12:40:24 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/stack.h"

void	trio(t_node *a)
{
	t_node	*iter;

	iter = a;
	if (a->data > a->next->data)
		swap(a, 'a');
	if (a->next->next && (a->next->data > a->next->next->data))
		rrotate(a, 'a', 1);
	while (iter->next)
	{
		if (iter->data > iter->next->data)
			trio(a);
		iter = iter->next;
	}
}

void	up_min(t_node *s)
{
	t_node	*s2;
	int		index;

	s2 = s;
	index = 0;
	sort_index(s);
	while (s && s->index != index)
		s = s->next;
	while (s2->index != index)
	{
		if (s->index2 >= (size_stack(s2) / 2))
			rrotate(s2, 'a', 1);
		else
			rotate(s2, 'a', 1);
	}
}

t_node	*five_sort(t_node *a, t_node *b)
{
	sort_index(a);
	up_min(a);
	b = push(a->data, b, 'b');
	a = pop(a);
	up_min(a);
	b = push(a->data, b, 'b');
	a = pop(a);
	trio(a);
	a = push(b->data, a, 'a');
	b = pop(b);
	a = push(b->data, a, 'a');
	b = pop(b);
	return (a);
}

t_node	*other_variation(t_node *a, t_node *b)
{
	int		size;
	t_node	*s;

	s = a;
	size = size_stack(a);
	if (size == 2)
	{
		if (a->data > a->next->data)
			swap(a, 'a');
	}
	else if (size == 3)
		trio(a);
	else if (size == 5)
		a = five_sort(a, b);
	else
	{
		b = insert2(a, b, size / 2, size);
		a = NULL;
		sort_index(b);
		a = insert3(a, b);
		size = size_stack(b);
	}
	return (a);
}

[![tr](https://img.shields.io/badge/turkish-red?label=language)](README.tr.md) [![en](https://img.shields.io/badge/english-white?label=language)](README.md) 
# Push_swap

**This is a project where integer arguments are taken and sorting is performed using specific commands and two stacks.**

In this project, sorting algorithms and stack structure are learned.

## Usage

```bash
git clone https://gitlab.com/Miko51/push_swap && cd push_swap 
make && ./push_swap <numbers>
```

The program primarily aims to sort lists of sizes 2 - 5, 100, and 500 elements. Therefore, it may yield inefficient results for lists outside of these ranges.

Example:

` ./push_swap 23 1 -90 46 13 `

It can also be a string. For example:

` ARG="23 1 -90 46 13"; ./push_swap $ARG | ./checker $ARG `

The correctness of your program is verified using the checker program provided by the project. Additionally, you can test your project with a visualized testing tool. You can access it from this [link](https://github.com/elijahkash/push_swap_gui).

## Score
86/100 [Project subject](en.subject.pdf)

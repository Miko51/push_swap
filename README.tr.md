[![tr](https://img.shields.io/badge/turkish-red?label=language)](README.tr.md) [![en](https://img.shields.io/badge/english-white?label=language)](README.md) 
# Push_swap

**Argüman olarak alınan tam sayıları belirli komutlarla ve iki adet stack ile sıralama yapılan bir projedir.**

Bu projede sıralama algoritmaları ve stack yapısı öğrenilir.

## Kullanım

```bash
git clone https://gitlab.com/Miko51/push_swap && cd push_swap 
make && ./push_swap <numbers>
```

Program ana olarak 2 - 5, 100 ve 500 elemanlık listeleri sıralamayı hedefler. Bu yüzden bu aralıklar haricindeki listeler için performanssız sonuç verebilir.

Örnek kullanım:

` ./push_swap 23 1 -90 46 13 `

String şeklinde de yollanabilir. Örneğin:

` ARG="23 1 -90 46 13"; ./push_swap $ARG | ./checker $ARG `

Projenin verdiği checker programı ile programınızın doğruluğu kontrol edilir.

Ayrıca projenizi görselleştirilmiş test aracıyla test edebilirsiniz. [Bu](https://github.com/elijahkash/push_swap_gui) linkten ulaşabilirsiniz

## Not
Projem %86 ile geçmektedir. [Projenin detaylı yönergesi](tr.subject.pdf)

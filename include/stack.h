/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: muhaaydi <muhaaydi@student.42kocaeli.com.  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/02 16:16:52 by muhaaydi          #+#    #+#             */
/*   Updated: 2023/03/03 15:13:11 by muhaaydi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STACK_H
# define STACK_H

# include <stdlib.h>
# include <limits.h>
# include <unistd.h>

typedef struct s{
	struct s	*prev;
	struct s	*next;
	int			index;
	int			index2;
	int			data;
}	t_node;

// char
char	**ft_split(char const *s, char c);
char	*ft_substr(char const *s, unsigned int start, size_t len);
// int
int		size_stack(t_node *a);
int		ft_atoi(const char *str);
int		is_up(t_node *s, int *space2, int j);
int		is_down(t_node *s, int *space2, int j);
int		ft_strcmp(char *s1, char *s2);
int		index_loc(t_node *s, int index);
int		control_args(char **args);
int		control_element(t_node *a, int *space2, int j);
int		rotate2(t_node *a, t_node *b, int c);
int		is_same(t_node *s);
// void
void	ft_putstr_fd(char *s, int fd);
void	sort_index(t_node *s);
void	rotate3(t_node *s, int size);
void	rotate(t_node *s, char c, int f);
void	rrotate(t_node *s, char c, int f);
void	swap(t_node *s, char c);
void	print_command(char *arr, char node);
void	push2(t_node **a, t_node **b, int *i, int *c);
void	trio(t_node *a);
void	free_list(t_node *s);
void	free_array(char **s);
void	multi_command(t_node *a, t_node *b, char *com);

// t_node
t_node	*compile_list(char **s);
t_node	*pop(t_node *s);
t_node	*push(int data, t_node *b, char c);
t_node	*insert(t_node *a, t_node *b);
t_node	*lst_last(t_node *s);
t_node	*five_sort(t_node *a, t_node *b);
t_node	*other_variation(t_node *a, t_node *b);
t_node	*insert3(t_node *a, t_node *b);
t_node	*insert2(t_node *a, t_node *b, int space, int size);

// size_t
size_t	ft_strlen(char *chr);
#endif

CC = gcc
CFLAGS = -Wall -Wextra -Werror
NAME = push_swap
RM = rm -f
SRC := $(shell find ./src -type f -name "*.c") 

OBJ =$(SRC:.c=.o)

%.o:%.c
	$(CC) $(CFLAGS) -c  $< -o $@

all:	$(NAME)

$(NAME):	$(OBJ)
	gcc $(OBJ) -o $(NAME)
	
clean:
	$(RM) $(OBJ)

fclean:	clean
	$(RM) $(NAME)

re:	fclean all

.PHONY: all fclean clean re
